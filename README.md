# pitch_HPS

Reference (please cite):
Sensory substitution reveals a manipulation bias
Anja T. Zai, Sophie Cave-Lopez, Manon Rolland, Nicolas Giret, Richard H.R. Hahnloser
DOI: https://doi.org/10.1038/s41467-020-19686-w
Website: https://gitlab.ethz.ch/songbird/pitch_hps

This code is licensed via the MIT License (see included file LICENSE).
Copyright: (c) 2020 ETH Zurich, Anja T. Zai, Richard H.R. Hahnloser 	

Work address: Institute of Neuroinformatics, Winterthurerstrasse 8057,
ETH Zurich and University of Zurich

Code for calculating the pitch of a waveform using a modified version of the harmonic power spectrum. 
Reference: Noll, A. Michael, Pitch determination of human speech by the harmonic power spectrum, the harmonic sum spectrum, and a maximum likelihood estimate, 1970

Example: see header of pitch_HPS.m

System requirements:
Matlab 2019b
Version of the software has been tested on Windows 10
No additional hardware necessary.
No toolbox required.
No additional .mat files required.


