function [pitch, nonoverlap, buffersize] = pitch_HPS(data, scanrate, varargin)
    %PITCH_HPS calculates the pitch of a waveform in Hz
    %   PITCH = PITCH_HPS(DATA, SCANRATE) returns the pitch values
    %   of sound waveform DATA where DATA was recorded using samplingrate
    %   SCANRATE. Each entry in PITCH corresponds to the pitch value in Hz
    %   from one buffer. Buffer size is set to 16 ms by default and the
    %   overlap of two buffers are by default zero. Default values are
    %   tuned for zebra finch song where the pitch is around 700 Hz.
    %
    %   [PITCH, NONOVERLAP] = PITCH_HPS(DATA, SCANRATE) returns the
    %   NONOVERLAP in samples between two buffers used to calculate pitch.
    %
    %   [PITCH, NONOVERLAP, BUFFERSIZE] = PITCH_HPS(DATA, SCANRATE) 
    %   returns the number of samples BUFFERSIZE used for the calculation 
    %   of each pitch value.
    %
    %   PITCH = PITCH_HPS(..., 'PARAM1',val1,'PARAM2',val2,...)
    %   specifies optional parameter name/value pairs to control the
    %   buffering of the data and the pitch calculation.
    %   The parameters are:
    %
    %   'buffersize'        - number of samples in data for buffering of the
    %                         data has to be an integer smaller than the
    %                         length of the data (default duration of a
    %                         buffer is set to 16 ms)
    %   'overlap'           - Integer number of overlapping (or underlapping)
    %                         samples in the buffered data frames (default = 0)
    %                         If overlap>0, there will be overlap number of
    %                         samples of datat from the end of the buffer
    %                         that will be repeated at the start of the
    %                         next buffer
    %                         If overlap<0, the buffering operation will
    %                         skip overlap number of samples of data after
    %                         each buffer, effectively skipping over data
    %                         and thus reducing the buffer "frame rate".
    %                         If overlap is empty or omitted overlap=0 (no
    %                         overlap or underlap).
    %   'energy_minimum'    - Minimum energy (positive number) in the
    %                         linear power spectrum below which values will
    %                         be ignored (default = 0.2)
    %   'index_minimum'     - Defines how many frequency bins (positive integer)
    %                         are considered (default = 7)
    %   'subsample_factor'  - factor for subsampling (default = 7)
    %   'upsample_factor'   - factor for upsampling (default = 7)
    %   'expected_pitch'    - expected pitch in Hz to correct for higher
    %                         harmonics. If the threshold is set correctyly,
    %                         this should not be necessary (default = 0)
    %   'offset'            - defines offset for logarithmic power spectrum
    %                         that is added to the linear power spectrum
    %                         before taking the logarithm (default = 0.1)
    %
    %
    % Example:
    %       [data, fs]=audioread('test.wav');
    %       figure,subplot(2,1,1),ylabel('Frequency (Hz)')
    %       [spec,f,t] = spectrogram(data,512,128,512,fs);
    %       imagesc(t',f(1:128),abs(spec(1:128,:))),axis xy,xlim([1 5])
    %       [pitch, nonoverlap] = pitch_HPS(data,fs,'energy_minimum',0.4,'expected_pitch',600);
    %       subplot(2,1,2),plot([1:length(pitch)]*nonoverlap/fs,pitch)
    %       xlabel('Time (s)'),ylabel('Pitch (Hz)'),xlim([1 5])
    %
    % ---
    % Copyright: (c) 2020 ETH Zurich, Anja T. Zai, Richard H.R. Hahnloser
    %
    % Reference (please cite):
    % Sensory substitution reveals a manipulation bias
    % Anja T. Zai, Sophie Cav?-Lopez, Manon Rolland, Nicolas Giret, 
    % Richard H.R. Hahnloser
    % DOI: https://doi.org/10.1038/s41467-020-19686-w
    %
    % Work address: Institute of Neuroinformatics, Winterthurerstrasse 8057,
    % ETH Zurich and University of Zurich
    % Website: https://gitlab.ethz.ch/songbird/pitch_hps
    
    % Subfunctions:
    %   buffered_data = buffer_data(vector, buffersize, overlap)
    %   pitch = get_pitch_HPS(buffer, energy_minimum, upsample_indices, subsample_factor, ...
    %           upsample_factor, index_minimum, freq_per_sample, expected_pitch,offset)
    %   [clipped_transform, upsample_array] = clip_and_interpolate(buffer, ...
    %           energy_minimum, upsample_indices, index_minimum)
    %   weighted_av = weighted_average(clipped_transform, max_index, ...
    %           index_minimum, subsample_factor)
    % MAT-files required: none
    %
    % See also: subfunction for more information about their function
    
    % References:
    %   [1] Noll, A. Michael, Pitch determination of human speech by the
    %       harmonic product sectrum, the harmonic sum spectrum, and a
    %       maximum likelihood estimate, 1970
    
    % Author: Joshua Herbst       Date: 2013                 Revision 0.0.0
    % Author: Richard Hahnloser   Date: 2013                 Revision 0.0.1
    % Author: Anja Zai            Date: 25-January-2019      Revision 1.0.0
    % Semantic Versioning (https://semver.org/): MAJOR.MINOR.PATCH

    % Last revision: 25-January-2019
    
    
    
    %------------- BEGIN CODE --------------
    
    % check input:
    if nargin<2
        error('Please specify DATA and SCANRATE.')
    end
    [n, p] = size(data);
    if isempty(data) || n>1 && p>1 || ~isnumeric(data) || ~isreal(data)
        error('Input DATA is not a vector of real numbers.')
    end
    if n<p
        data = data';
    end
    if isempty(scanrate) || ~isnumeric(scanrate) || ~isreal(scanrate) || length(scanrate)>1 || scanrate<0 || mod(scanrate,1)~=0
        error('Input SCANRATE has to be a positive integer.')
    end
    
    % Parse arguments and check if parameter/value pairs are valid
    paramNames = {'buffersize','overlap','energy_minimum','index_minimum',...
        'subsample_factor','upsample_factor','expected_pitch','offset'};
    defaults   = {floor(scanrate/1000*16),0,0.2,7,7,7,0,0.1};
    
    [buffersize, overlap, energy_minimum,index_minimum, subsample_factor,...
        upsample_factor, expected_pitch, offset]...
        = internal.stats.parseArgs(paramNames, defaults, varargin{:});
    
    if isempty(buffersize) || ~isnumeric(buffersize) || ~isreal(buffersize) || buffersize<0 || mod(buffersize,1)~=0 || buffersize>length(data)
        error('BUFFERSIZE has to be a positive integer smaller than the size of the input vector.')
    end
    if isempty(overlap) || ~isnumeric(overlap) || ~isreal(overlap) || overlap<0 || mod(overlap,1)~=0 || overlap>=buffersize
        error('OVERLAP has to be a positive integer smaller than buffersize.')
    end
    if isempty(energy_minimum) || ~isnumeric(energy_minimum) || ~isreal(energy_minimum) || energy_minimum<0
        error('ENERGY_MINIMUM has to be positive.')
    end
    if isempty(index_minimum) || ~isnumeric(index_minimum) || ~isreal(index_minimum) || index_minimum<0 || mod(index_minimum,1)~=0 || 2^index_minimum>buffersize
        error('INDEX_MINIMUM has to be a positive integer and 2^index_minimum has to be smaller than buffersize')
    end
    if isempty(subsample_factor) || ~isnumeric(subsample_factor) || ~isreal(subsample_factor) || subsample_factor<0 || mod(subsample_factor,1)~=0
        error('SUBSAMPLE_FACTOR has to be a positive integer')
    end
    if isempty(upsample_factor) || ~isnumeric(upsample_factor) || ~isreal(upsample_factor) || upsample_factor<0 || mod(upsample_factor,1)~=0
        error('UPSAMPLE_FACTOR has to be a positive integer')
    end
    if isempty(expected_pitch) || ~isnumeric(expected_pitch) || ~isreal(expected_pitch) || expected_pitch<0
        error('EXPECTED_PITCH has to be positive')
    end
    if isempty(offset) || ~isnumeric(offset) || ~isreal(offset)
        error('OFFSET has to be a real number.')
    end
    
    % buffer the data into windows and calculate logarithmic power spectrum
    buff = buffer_data(data, buffersize, overlap);
    num_buffs = size(buff, 2);
    nonoverlap = buffersize - overlap; % XXX
    for i = 1 : num_buffs
        buff(:, i) = abs(fft2(hamming(length(buff(:, i))).*buff(:, i)))';
    end
    buff = log(buff + offset);
    
    freq_n=2^index_minimum;

    upsample_indices = round(1:1/upsample_factor:freq_n);
    
    if max(max(buff))<=log(energy_minimum)
        error('The energy_minimum is too high so that everything is clipped.')
    end
    
    % initialize output
    pitch = zeros(1, num_buffs);
    % loop over all buffers and calculate pitch
    for buff_index = 1:num_buffs
        buffer = buff(1:freq_n, buff_index);
        % calculate pitch for single buffer
        pitch(buff_index) = get_pitch_HPS(buffer, energy_minimum, ...
            upsample_indices, subsample_factor, upsample_factor, ...
            index_minimum, scanrate/buffersize, expected_pitch);
    end
end


%----------------Subfucntions--------------------------------------------

function buffered_data = buffer_data(vector, buffersize, overlap)
    % buffer_data buffers a signal vector into a matrix of data frames.
    %    Y = buffer_data(X,N) partitions signal vector X into nonoverlapping data
    %    segments (frames) of length N.  Each data frame occupies one
    %    column in the output matrix, resulting in a matrix with N rows.
    %
    %    Y = buffer_data(X,N,P) specifies an integer value P which controls the amount
    %    of overlap or underlap in the buffered data frames.
    %    - If P>0, there will be P samples of data from the end of one frame
    %      (column) that will be repeated at the start of the next data frame.
    %    - If P<0, the buffering operation will skip P samples of data after each
    %      frame, effectively skipping over data in X, and thus reducing the
    %      buffer "frame rate".
    %    - If empty or omitted, P is assumed to be zero (no overlap or underlap).
    
    non_overlap = buffersize - overlap;
    vectorsize = size(vector);

    % calculate the number of buffers
    number_of_buffers = floor((vectorsize(1)-buffersize)/non_overlap)+1;
    
    %initialize bufferd_data array
    buffered_data = zeros(buffersize, number_of_buffers);
    
    % buffering data
    for count = 0:number_of_buffers-1
        buffered_data(:,count+1) = ...
            vector(count*non_overlap+1:count*non_overlap+buffersize);
    end
end

function pitch = get_pitch_HPS(buffer, energy_minimum, upsample_indices, ...
        subsample_factor, upsample_factor, index_minimum, freq_per_sample, expected_pitch)
    % get_pitch_HPS: main pitch calculation function for a single buffer
    %
    % inputs: as in main function
    % outputs: pitch of single input buffer (Hz)
    
    % (1) clip to minimal value and upsample
    [clipped_transform, upsample_array] = clip_and_interpolate(buffer, energy_minimum, upsample_indices, index_minimum);
    
    % (2) find maximum index of that upsampled vector
    max_index = find_max_index(upsample_array, subsample_factor, upsample_factor);
    
    % (3) calculate a weighted average around that sample
    weighted_av = weighted_average(clipped_transform, max_index, index_minimum, subsample_factor);
    
    % (4) what pitch does that correspond to
    pitch = weighted_av * freq_per_sample;
    
    % correct for higher harmonics (should not be necessary if threshold is set
    % correctly)
    if expected_pitch > 0
        harmonic_indices = round(pitch/expected_pitch);
        pitch=pitch./max(1,harmonic_indices);
    end
end

function [clipped_transform, upsample_array] = clip_and_interpolate(buffer, energy_minimum, upsample_indices, index_minimum)
    % clip buffer at energy minimum and shift values to positive domain
    buffer(1 : index_minimum - 1) = log(energy_minimum);
    buffer = max(buffer, log(energy_minimum)) - log(energy_minimum);
    % clipped transform to exponential domain, >=1
    clipped_transform = exp(buffer);
    % upsample and add 1, so the upsampled version is >=1
    upsample_array = buffer(upsample_indices)+1;
end

function max_index = find_max_index(upsample_array, subsample_factor, upsample_factor)
    % find_max_index: finds the index of the frequency bin corresponding to the
    % HPS peak
    % inputs: as in main function
    % outputs: fequency bin index of the HPS peak
    
    % multiply upsample array with subsampled versions of itself
    array_length = length(upsample_array);
    upsample_array_tmp = upsample_array;
    for count = 2:subsample_factor
        tmp_array_length = floor(array_length/count);
        upsample_array(1:tmp_array_length) = upsample_array(1:tmp_array_length).*...
            upsample_array_tmp(1:count:1+count*(tmp_array_length-1));
    end
    % find index of maximum
    [~, index] = max(upsample_array,[],1);
    % divide by upsample factor. index 1 is 0 Hz, therefore we shift index by 1
    max_index = (index-1)/upsample_factor+1;
end

function weighted_av = weighted_average(clipped_transform, max_index, index_minimum, subsample_factor)
    % calculate weighted average around max_index
    if max_index < index_minimum
        weighted_av = 0;
        return
    end
    max_index_freq = max_index-1;
    num_points = length(clipped_transform);
    
    sides = 2;
    bands = 3;
    tot_bands = sides*bands+1;
    value = zeros(subsample_factor,tot_bands);
    weight = zeros(subsample_factor,tot_bands);
    
    count_max = min(floor((num_points-bands-1)/max_index_freq),subsample_factor);
    for count = 1:count_max
        c = round(count*max_index_freq)-bands;
        indices = (1:tot_bands)'+c;
        weight(count,:) = clipped_transform(indices)-1+eps;
        value(count,:) = ((1:tot_bands)+c-1)/count.*weight(count,:);
    end
    weighted_av = sum(sum(value))/sum(sum(weight));
end


